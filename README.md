# CSU11021 / CSU11022 Introduction to Computing I & II

In general, you can put your `.s` assembly language programs anywhere in this folder. Each program will usually have a main assembly language source file (e.g. `1001-first.s`) and often a second test initialisation file (e.g. `1001-first_test.s`). **It is important that you keep both files in the same folder.**

It is recommended that you organise each program into a sepaarte folder, for example, the `1001-first` program should be in the `1001-first` folder, `1010-add` should be in the `1010-add` folder, and so on. This will make it easy to focus on one program at a time and also to maintain different versions of the same program if you want to ([see below](#make-duplicates-of-a-program)).

## Adding a new program

1. Create a new folder for the program
    - Right click on some empty space in the file explorer and select "New Folder ...".
    - Give the folder a name.
2. Copy any template files you were given to the new folder. (Usually this will be XXXX.s and XXXX_test.s)
    - Drag the files into the new folder or, if using the Coder web IDE, right-click the folder and select "Upload ...").

## Making duplicates of a program

The recommended approach is to create new sub-folders for each duplicate inside the main folder for the program.

For example, for `1010-add`:

1. Create a new folder called `v1` inside the `1010-add` folder and move (drag) `1010-add.s` and `1010-add_test.s` into this `v1` folder.

2. To make a duplicate of `1010-add`, right-click the new `v1` folder and select "Copy" then right-click the `1010-add` folder and select "Paste"