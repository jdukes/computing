  .syntax unified
  .cpu cortex-m3
  .fpu softvfp
  .thumb
  
  .global  Main

Main:

  @ A program to compute 4+5
  MOV   R1, #4
  MOV   R2, #5
  ADD   R0, R1, R2

End_Main:
  BX    LR

  .end